#+TITLE ios receipes

* ios receipes 

* package manager to manage objective c libraries
  Ubuntu has apt, mac-osx has brew and objective-C libraries have [[http://cocoapods.org/][cocoapods]].

** Without cocopods
   When you want to use a third party objective-c library, the long way is to clone
   the github repository and copy the .h and .m files into your
   xcode project.

** Using libraries with cocoapods
   Cocoapods is a package manager for objective c libraries. 

+ First install cocoapods

  It took some time for me and wasn't sure if it was doing anything
  at all or hanging. Try the optional verbose switch to see what is
  happening under the covers.

#+BEGIN_SRC sh

# install cocoapods via ruby gem
sudo gem install cocoapods -V

# setup cocoapods
pod setup

# install the pod
sudo gem install 
#+END_SRC

