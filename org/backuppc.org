#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />

#+TITLE: Backup VPS using BackupPC

* Problem
  Do you need to backup vps. Long term you need a backup strategy in place. Not the
  latest fad or shiny hack, but reliable, space conscious, revolving long term 
  storage.


* Requirements
- able to backup entire machine, including OS
- able to restore entire machine
- keep a history of backups.
- incremental backups.


* Solution
  - Most VPS's provide a backup feature of some sort. 
  - But responsiblity for the backups falls on you.
  - Lots of off the shelf backup solutions.
  - I settled on [[http://backuppc.sourceforge.net/][Backup PC]], a rsync based solution with a crude frontend written
    in perl.


** Setup
- Dedicated machine with plenty of gig space.
- I used ramnode's $2/month plan giving 50GB of space.
- With promotional code, the server end up $15/year for a dedicated backup pc

** Install
- Follow the digitalocean tutorial [[https://www.digitalocean.com/community/articles/how-to-use-backuppc-to-create-a-backup-server-on-an-ubuntu-12-04-vps][here]]
- Setup User
   - If you don't want to login to your remote machines as root
   - Setup a user on the client you want to backup.
   - Allow backuppc user via ssh 
   - Setup passwordless login
     #+BEGIN_SRC sh
     ssh-copy-id -i ~/.ssh/id_rsa.pub '-p 25000 backuppc@hostname.com'     
     #+END_SRC

   - Give sudo access to rsync command on client.
   - Update backuppc host to run rsync backup as the new user.
   - Notes for setting up a dedicated backup user on a debian distro [[https://www.debian-administration.org/article/588/][here]].


* Troubleshooting
** Verify passwordless login works

   #+BEGIN_SRC sh
       ssh -p 25550 -l backuppc softwareprototyp.com whoami
   #+END_SRC 

** run the actual command itself as backuppc user 
   command is in xfer logs

** add -vvv in $rsyncCommand to get details


** for freebsd, might have to add the -t for ssh in $rsyncCommand
   - only needed if your freebsd backuppc user get a 

   #+BEGIN_EXAMPLE
     sudo: no tty present and no askpass program specified
   #+END_EXAMPLE

   - For freebsd, rsync is in /usr/local/bin/rsync 
     - Update config file for host only (not global config) to overwrite the rsync
     command variable in xfer.

   -  The exclude directories for freebsd are different from Ubuntu
   #+BEGIN_SRC sh
     Sent exclude: /proc

     Sent exclude: /sys 
     Sent exclude: /usr/ports
     Sent exclude: /usr/doc
     Sent exclude: /usr/share/doc 
     Sent exclude: /tmp
     Sent exclude: /dev
     Sent exclude: /var/db/mysql
   #+END_SRC

