#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
#+TITLE: Python Notes


* Django + Nginx + Uwsgi
** fireup new server
** create a user

       - setup password

*** add user to sudo group
    - on debian, install sudo first
      - sudo apt-get install sudo
    - create home directory
    - might have to add permissions of user to home directory
** create a virtual env 
   - install pip via python-pip pkackage
   - use pip to install virtualenv
   - add ssh key to git repo so server can do a git pull
   - Init DB
  
** Levergae fabric to automate deployments
** Optional: Setup cron job for django-yubin 

* Deleting Packages installed via python setup.py install
  
- When you install packages not using a package manager, you have to remove
  the files manually and undo any changes the installation did manually.

- It's much easier if you record the list of all files bby reinstalling with
  an option.

  #+BEGIN_SRC sh
  > python setup.py install --record files.txt
  #+END_SRC

-  Once you want to uninstall you can use xargs to do the removal:
  #+BEGIN_SRC sh
  > cat files.txt | xargs rm -rf
  #+END_SRC


* Using South on Existing Django Project
** install south

       #+BEGIN_SRC python
         pip install south
       #+END_SRC 

** add south to settings.py

       #+BEGIN_SRC python
           INSTALLED_APPS = (
               ...
               'south',
               ...
           )
       #+END_SRC

** Run syncdb so south tables get created

       #+BEGIN_SRC python
           python manage.py syncdb
       #+END_SRC

** Convert Existing app to Django South
       #+BEGIN_SRC python
           python manage.py convert_to_south myproject.myapp
       #+END_SRC


** Change the models & then
#+BEGIN_SRC python
python manage.py schemamigration myproject.myapp --auto
python manage.py migrate myproject.myapp
#+END_SRC

** Verify your DB has Changed and your app starts like usual.

** Troubleshooting
*** All data is stored in the migrations folder.
*** Delete the migrations folder in your app to start again with Django South


* django using gunicorn
** Install gunicorn in your virtual environment

#+BEGIN_SRC sh
pip install gunicorn
#+END_SRC

** Add gunicorn as an app in your settings.py
   
** test it locally

#+BEGIN_SRC sh
$ ./manage.py run_gunicorn
#+END_SRC

or

#+BEGIN_SRC sh
$ gunicorn_django -b 0.0.0.0:8000
#+END_SRC








* Debugging Python/Django
  Put those lines where you want to get a break :
  import pdb; pdb.set_trace()

  run the server with python 
  manage.py runserver -p 8888

  Go to page with your web browser (enter the address of
  the host or localhost if the server is running on same computer of your web
  browser) and add the port (ie. http://localhost:8888 ). Your done, once you go
  to the page where the set_trace() method is read, the server will break at this
  position and the terminal where you run the server will show you the prompt,
  allowing you to go trough your code and variable


* Python Links I keep on coming back to

- [[http://strftime.org/][Formatting Date String]]
